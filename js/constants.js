const configuration = {
    apiUrl: 'https://www.thecocktaildb.com/api/json/v1',
    // development API key (1) has limited number of objects in response
    apiKey: '1',

    navIngredients: ['Vodka', 'Rum', 'Gin', 'Tequila', 'Whiskey'],
    navGlasses: ['Highball glass', 'Cocktail glass', 'Old-fashioned glass', 'Whiskey Glass', 'Collins glass', 'Shot glass'],
}

const navToggleA = document.querySelector('#navToggle');
const navTilesUl = document.querySelector('#navTiles');

const ingredientsNavUl = document.querySelector('#ingredients-nav');
const categoriesNavUl = document.querySelector('#categories-nav');
const glassesNavUl = document.querySelector('#glasses-nav');
const alcoholicNavUl = document.querySelector('#alcoholic-nav');

const pageMain = document.querySelector('#page');
const pageTitleH1 = document.querySelector('#page-title');
const pageContentDiv = document.querySelector('#page-content');
const pageLoadingH3 = document.querySelector('#page-loading');
