String.prototype.replaceAll = function(from, to) {
    return this.replace(new RegExp(`${from}`, 'g'), to);
}

class Cocktail {
    constructor(drinkData) {
        this.id = drinkData.idDrink;
        this.name = drinkData.strDrink;
        this.tags = drinkData.strTags ? drinkData.strTags.split(',') : [];
        this.category = drinkData.strCategory;
        this.iba = drinkData.strIBA;
        this.glass = drinkData.strGlass;
        this.instructions = {
            en: drinkData.strInstructions,
            es: drinkData.strInstructionsES,
            de: drinkData.strInstructionsDE,
            fr: drinkData.strInstructionsFR
        }
        this.image = drinkData.strDrinkThumb;

        this.ingredients = [];
        let ingredientIndex = 1;
        while (drinkData[`strIngredient${ingredientIndex}`]) {
            this.ingredients.push({
                name: drinkData[`strIngredient${ingredientIndex}`],
                measure: drinkData[`strMeasure${ingredientIndex}`]
            });
            ingredientIndex++;
        }

        this.modified = new Date(drinkData.dateModified);
    }

    thumbnail() {
        return `${this.image}/preview`;
    }

    createPreview() {
        return htmlToElement(`
            <article title="${this.name}" class="preview">
                <a href="#cocktail?i=${this.id}">
                    <h3 class="nowrap-ellipsis">${this.name}</h3>
                    <hr/>
                    <img class="thumbnail" src="${this.thumbnail()}" alt="${this.name} photo">
                </a>
            </article>`);
    }

    createDetails() {
        return htmlToElement(`
            <div class="cocktail">
                <div class="split">
                    <div class="split-col split-sm">
                        <img class="cocktail-photo circle" src="${this.image}" alt="${this.name}">
                    </div>
                    <div class="split-col cocktail-ingredients-container">
                        <h2>Ingredients</h2>
                        <ul id="cocktail-ingredients" class="ingredients"></ul>
                    </div>
                </div>
                <div class="preparation">
                    <h2>Preparation</h2>
                    <p class="paragraph">${this.instructions.en}</p>
                </div>
            </div>`);
    }
}

class Ingredient {
    constructor(ingredientData) {
        this.name = ingredientData.strIngredient1;
    }

    code() {
        return this.name.replaceAll(' ', '_')
    }

    createPreview() {
        return htmlToElement(`
            <article title="${this.name}" class="preview">
                <a href="#ingredient?i=${this.code()}">
                    <h3 class="nowrap-ellipsis">${this.name}</h3>
                    <hr/>
                    <img class="thumbnail" src="https://www.thecocktaildb.com/images/ingredients/${this.name}-Small.png" 
                        alt="${this.name} photo">
                </a>
            </article>`);
    }
}

class Glass {
    constructor(glassData) {
        this.name = glassData.strGlass;
    }

    code() {
        return this.name.replaceAll(' ', '_')
    }

    createPreview() {
        return htmlToElement(`
            <article title="${this.name}" class="preview">
                <a href="#glass?g=${this.code()}">
                    <h3 class="nowrap-ellipsis">${this.name}</h3>
                    <hr/>
                    <img class="thumbnail" src="https://via.placeholder.com/150?text=${this.name}" 
                        alt="${this.name}">
                </a>
            </article>`);
    }
}

class Page {
    constructor(hash) {
        let hashIndex = hash.indexOf('#');
        if (hashIndex === -1) {
            this.resource = 'home';
            return;
        }

        let queryIndex = hash.indexOf('?');

        if (queryIndex > -1) {
            this.resource = hash.substring(hashIndex + 1, queryIndex);
        } else {
            this.resource = hash.substring(hashIndex + 1);
        }

        if (queryIndex > -1) {
            this.params = {};

            hash.substring(queryIndex + 1)
                .split('&')
                .forEach(pair => {
                    let split = pair.split('=');
                    this.params[split[0]] = split[1];
                })
        }
    }

    getParam(name) {
        return this.params[name].replaceAll('_', " ");
    }
}