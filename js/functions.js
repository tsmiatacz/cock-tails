async function getData(endpoint, params) {
    let url = `${configuration.apiUrl}/${configuration.apiKey}/${endpoint}.php`;

    if (params) {
        const queryString = Object.entries(params)
            .map(entry => `${entry[0]}=${entry[1]}`)
            .join('&');
        url += `?${queryString}`;
    }

    const response = await fetch(url);
    return await response.json();
}

function createNavLink(page, queryKey, queryValue) {
    const li = document.createElement('li');
    const a = document.createElement('a');

    a.href = `#${page}?${queryKey}=${queryValue.replaceAll(' ', '_')}`;
    a.innerText = queryValue;

    li.appendChild(a);
    return li;
}

function createNavGroup(navUl, page, queryKey, valueField) {
    const params = {};
    params[queryKey] = 'list';

    getData('list', params)
        .then(({drinks}) => {
            drinks.forEach(value => {
                const li = createNavLink(page, queryKey, value[valueField]);
                navUl.appendChild(li);
            })
        })
        .catch(err => {
            console.error(err);
        });
}

async function getRandomCocktails(count) {
    const cocktails = [];

    while (cocktails.length < count) {
        const data = await getData('random');
        if (!cocktails.some(c => c.id === data.drinks[0].idDrink)) {
            cocktails.push(new Cocktail(data.drinks[0]));
        }
    }

    return cocktails;
}

function setLocale(lang='en') {
    document.cookie = `locale=${lang}`;
}

function htmlToElement(html) {
    const template = document.createElement('template');
    template.innerHTML = html.trim();
    return template.content.firstChild;
}

function htmlToElements(html) {
    const template = document.createElement('template');
    template.innerHTML = html.trim();
    return template.content.children;
}

async function renderPage() {
    pageMain.classList.add('hidden');
    pageTitleH1.innerHTML = '';
    pageContentDiv.innerHTML = '';
    pageLoadingH3.classList.remove('display-none');

    const page = new Page(window.location.hash);
    const pageRouting = routing[page.resource];

    pageTitleH1.innerText = pageRouting.title(page);

    if (pageRouting.staticContent) {
        pageContentDiv.innerHTML = pageRouting.staticContent;
    }

    if (pageRouting.render) {
        await pageRouting.render(page);
    }
    pageLoadingH3.classList.add('display-none');

    pageMain.classList.remove('hidden');
}