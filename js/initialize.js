configuration.navIngredients.forEach(ingredient => {
    const li = createNavLink('ingredient', 'i', ingredient);
    ingredientsNavUl.appendChild(li);
})

configuration.navGlasses.forEach(glass => {
    const li = createNavLink('glass', 'g', glass);
    glassesNavUl.appendChild(li);
})

createNavGroup(categoriesNavUl, 'category', 'c', 'strCategory');
createNavGroup(alcoholicNavUl, 'alcoholic', 'a', 'strAlcoholic');

navToggleA.addEventListener('click', () => {
    navTilesUl.classList.toggle('nav-hidden');
})

renderPage();
window.addEventListener('hashchange', async () => {
    navTilesUl.classList.add('nav-hidden');
    await renderPage();
})