routing = {
    home: {
        title: () => 'Welcome to cock-tails!',
        render: renderHomePage,
        staticContent: `
            <div class="split">
                <div class="split-col order-second">
                    <p>
                    <span>
                    <section>
                    <h3>What is this?</h3>
                    <p class="paragraph">This website is a database of alcoholic and non-alcoholic cocktails. There's a photo, list of ingredients and recipe for every single cocktail. 
                        All recipes are split into several categories that you can select from top navigation bar.</p>
                    <p class="paragraph">It's a responsive Single Page Application project created in pure HTML5+CSS+JS. 
                        No external frameworks or libraries were used except <a href="https://fontawesome.com/" target="_blank">Font Awesome.</a></p>
                    </section>
                    <h3>Data source</h3>
                    <p class="paragraph">Cocktail data is fetched from <a href="https://www.thecocktaildb.com/api.php?ref=apilist.fun" target="_blank">TheCocktailDB</a>, an open, crowd-sourced database of drinks and cocktails from around the world. </p>
                    <p class="paragraph">In some cases API output size might be limited by the usage of development API key.</p>
                    </span>
                    </p>
                </div>
                <div class="split-col split-sm order-first">
                    <img src="img/helena-yankovska-sJOy9pveFbU-unsplash.jpg" class="cover-img" alt="bartender">
                </div>
            </div>
            <h2>Wanna try something new? Check out one of these!</h2>
            <div id="random-cocktails" class="results"></div>`
    },
    ingredients: {
        title: () => 'List of ingredients',
        render: renderList
    },
    ingredient: {
        title: page => `Ingredient: ${page.getParam('i')}`,
        render: renderFilter
    },
    category: {
        title: page => `Category: ${page.getParam('c')}`,
        render: renderFilter
    },
    glasses: {
        title: () => 'List of glasses',
        render: renderList
    },
    glass: {
        title: page => `Glass: ${page.getParam('g')}`,
        render: renderFilter
    },
    alcoholic: {
        title: page => `${page.getParam('a')} cocktails`,
        render: renderFilter
    },
    about: {
        title: () => 'About',
        staticContent: `
            <p class="paragraph">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, distinctio dolorem dolorum ea enim error
                excepturi harum, illo libero nihil nisi ratione, soluta vero! Deleniti fuga fugiat pariatur praesentium
                quidem.</p>`
    },
    cocktail: {
        title: () => 'Cocktail',
        render: renderDetails
    }
}

async function renderHomePage() {
    const randomCocktailsDiv = document.getElementById('random-cocktails');

    const cocktails = await getRandomCocktails(5);
    cocktails.forEach(cocktail => {
        randomCocktailsDiv.appendChild(cocktail.createPreview());
    });
}

async function renderList(page) {
    const resultsDiv = document.createElement('div');
    resultsDiv.classList.add('results');

    const data = await getData('list', page.params);
    data.drinks.forEach(objData => {
        let obj;
        if (page.resource === 'ingredients') {
            obj = new Ingredient(objData)
        } else if (page.resource === 'glasses') {
            obj = new Glass(objData);
        }

        if (obj) {
            resultsDiv.appendChild(obj.createPreview());
        }
    })
    pageContentDiv.appendChild(resultsDiv);
}

async function renderFilter(page) {
    const resultsDiv = document.createElement('div');
    resultsDiv.classList.add('results');

    const data = await getData('filter', page.params);
    data.drinks.forEach(drink => {
        const cocktail = new Cocktail(drink);
        resultsDiv.appendChild(cocktail.createPreview());
    });

    pageContentDiv.appendChild(resultsDiv);
}

async function renderDetails(page) {
    const data = await getData('lookup', page.params);
    const cocktail = new Cocktail(data.drinks[0]);

    pageTitleH1.innerText = cocktail.name;
    pageContentDiv.appendChild(cocktail.createDetails());

    const ingredientsUl = document.getElementById('cocktail-ingredients');
    cocktail.ingredients.forEach(i => {
        const li = document.createElement('li');
        li.innerText = `${i.name}`
        if (i.measure) {
            li.innerText += ` – ${i.measure}`
        }
        ingredientsUl.appendChild(li);
    })
}

