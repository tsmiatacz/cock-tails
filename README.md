# cock-tails

This is a sample static Single Page Application demo.  
SPA is based on (#) URI fragments so no additional server configuration is required.   
No external frameworks or libraries were used except [Font Awesome](https://fontawesome.com/).  

### Preview

Page is automatically deployed to:  
[https://cock-tails.netlify.app](https://cock-tails.netlify.app)  

### API

Application is using [TheCocktailDB API](https://www.thecocktaildb.com/api.php) to fetch cocktail data.  
In some cases API output size might be limited by the usage of development API key.